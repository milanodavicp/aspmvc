﻿CREATE TABLE [dbo].[Proizvod]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ProizvodId] INT NULL, 
    [naziv] NVARCHAR(50) NOT NULL, 
    [opis] NVARCHAR(50) NULL, 
    [kategorija] NVARCHAR(50) NULL, 
    [proizvodjac] NVARCHAR(50) NULL, 
    [dobavljac] NVARCHAR(50) NULL, 
    [cena] INT NULL
)
