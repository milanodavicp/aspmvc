﻿using DataLibrary.DataAccess;
using DataLibrary.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLibrary.BusinessLogic
{

    public class RootProizvodModel
    {
        public List<ProizvodModel> ProizvodModel { get; set; }
    }

    public static class ProizvodProcessor
    {
        

        private static int DbType = 1; // 1 - sql, 2 - json
        private static string JsonPath = @"C:\Users\odavi\source\repos\MVCApp\json_db.txt";
        public static int CreateProizvod(int _proizvodId, string _naziv, string _opis, string _kategorija, string _proizvodjac, string _dobavljac, int _cena) 
        {

            ProizvodModel data = new ProizvodModel
            {
                ProizvodId = _proizvodId,
                naziv = _naziv,
                opis = _opis,
                kategorija = _kategorija,
                proizvodjac = _proizvodjac,
                dobavljac = _dobavljac,
                cena = _cena
            };

            if (ProizvodProcessor.DbType == 1)
            {
                string sql = @"INSERT INTO dbo.Proizvod (ProizvodId,naziv,opis,kategorija,proizvodjac,dobavljac,cena)
                            VALUES (@ProizvodId,@naziv,@opis,@kategorija,@proizvodjac,@dobavljac,@cena);";

                return SqlDataAccess.SaveData(sql, data);
            }
            else if (ProizvodProcessor.DbType == 2)
            {

                string get_json = System.IO.File.ReadAllText(ProizvodProcessor.JsonPath);
                var result = JsonConvert.DeserializeObject<RootProizvodModel>(get_json);

                List<ProizvodModel> _data = result.ProizvodModel;
                data.Id = _data[0].Id + 1;
                _data.Insert(0,data);

                string json = "{\"ProizvodModel\":" + JsonConvert.SerializeObject(_data.ToArray()) + "}";
                System.IO.File.WriteAllText(ProizvodProcessor.JsonPath, json);

                return 1;
            }

            return 0;

        }

        public static List<ProizvodModel> LoadProizvode()
        {

            List<ProizvodModel> proizvodi = null;

            if (ProizvodProcessor.DbType == 1)
            {
                string sql = @"SELECT * 
                            FROM dbo.Proizvod;";
                proizvodi = SqlDataAccess.LoadData<ProizvodModel>(sql);
            }
            else if (ProizvodProcessor.DbType == 2)
            {
                string get_json = System.IO.File.ReadAllText(ProizvodProcessor.JsonPath);
                var result = JsonConvert.DeserializeObject<RootProizvodModel>(get_json);
                proizvodi = result.ProizvodModel;
            }

            return proizvodi;
        }

        public static ProizvodModel LoadOneProizvod(int id)
        {

            ProizvodModel proizvod = null;

            if (ProizvodProcessor.DbType == 1)
            {
                string sql = @"SELECT * 
                            FROM dbo.Proizvod as p
                            WHERE p.Id = " + id + ";";
                proizvod = SqlDataAccess.LoadData<ProizvodModel>(sql)[0];
            }
            else if (ProizvodProcessor.DbType == 2)
            {
                string get_json = System.IO.File.ReadAllText(ProizvodProcessor.JsonPath);
                var result = JsonConvert.DeserializeObject<RootProizvodModel>(get_json);
                List<ProizvodModel> proizvodi = result.ProizvodModel;

                foreach (var row in proizvodi)
                {
                    if (row.Id == id)
                    {
                        return row;
                    }
                }
            }

            return proizvod;
        }


        public static int EditProizvod(int _id, int _proizvodId, string _naziv, string _opis, string _kategorija, string _proizvodjac, string _dobavljac, int _cena)
        {

            ProizvodModel data = new ProizvodModel
            {
                Id = _id,
                ProizvodId = _proizvodId,
                naziv = _naziv,
                opis = _opis,
                kategorija = _kategorija,
                proizvodjac = _proizvodjac,
                dobavljac = _dobavljac,
                cena = _cena
            };

            if (ProizvodProcessor.DbType == 1)
            {
                string sql = @"UPDATE dbo.Proizvod
                               SET ProizvodId=@ProizvodId,naziv=@naziv,opis=@opis,kategorija=@kategorija,proizvodjac=@proizvodjac,dobavljac=@dobavljac,cena=@cena
                               WHERE Id = @Id;";

                return SqlDataAccess.SaveData(sql, data);
            }
            else if (ProizvodProcessor.DbType == 2)
            {
                string get_json = System.IO.File.ReadAllText(ProizvodProcessor.JsonPath);
                var result = JsonConvert.DeserializeObject<RootProizvodModel>(get_json);
                List<ProizvodModel> proizvodi = result.ProizvodModel;

                foreach (var row in proizvodi)
                {
                    if (row.Id == _id)
                    {
                        row.ProizvodId = _proizvodId;
                        row.naziv = _naziv;
                        row.opis = _opis;
                        row.kategorija = _kategorija;
                        row.proizvodjac = _proizvodjac;
                        row.dobavljac = _dobavljac;
                        row.cena = _cena;
                        break;
                    }
                }

                string json = "{\"ProizvodModel\":" + JsonConvert.SerializeObject(proizvodi.ToArray()) + "}";
                System.IO.File.WriteAllText(ProizvodProcessor.JsonPath, json);

                return 1;
            }

            return 0;
        }
    }
}
