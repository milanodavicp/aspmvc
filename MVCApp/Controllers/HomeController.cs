﻿using MVCApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLibrary;
using DataLibrary.BusinessLogic;

namespace MVCApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult ViewProizvode()
        {
            ViewBag.Message = "Lista Proizvoda:";

            var data = ProizvodProcessor.LoadProizvode();

            List<ProizvodiModel> proizvodi = new List<ProizvodiModel>();

            foreach (var row in data)
            {
                proizvodi.Add(new ProizvodiModel
                {
                    Id = row.Id,
                    ProizvodId = row.ProizvodId,
                    naziv = row.naziv,
                    opis = row.opis,
                    kategorija = row.kategorija,
                    proizvodjac = row.proizvodjac,
                    dobavljac = row.dobavljac,
                    cena = row.cena
                }); ;
            }

            return View(proizvodi);
        }

        public ActionResult UnesiProizvod()
        {
            ViewBag.Message = "Unesi proizvod.";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UnesiProizvod(ProizvodiModel model)
        {
            if (ModelState.IsValid) 
            {
                ProizvodProcessor.CreateProizvod(model.ProizvodId, model.naziv, model.opis, model.kategorija, model.proizvodjac, model.dobavljac, model.cena);

                return RedirectToAction("Index");
            }

            return View();
        }

        public ActionResult EditProizvode(int id)
        {
            ViewBag.Message = "Edit proizvode.";

            var data = ProizvodProcessor.LoadOneProizvod(id);
            ProizvodiModel proizvod = new ProizvodiModel();
            proizvod.Id = data.Id;
            proizvod.ProizvodId = data.ProizvodId;
            proizvod.naziv = data.naziv;
            proizvod.opis = data.opis;
            proizvod.kategorija = data.kategorija;
            proizvod.proizvodjac = data.proizvodjac;
            proizvod.dobavljac = data.dobavljac;
            proizvod.cena = data.cena; 

            return View(proizvod);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProizvode(ProizvodiModel model)
        {
            if (ModelState.IsValid)
            {
                ProizvodProcessor.EditProizvod(model.Id, model.ProizvodId, model.naziv, model.opis, model.kategorija, model.proizvodjac, model.dobavljac, model.cena);

                return RedirectToAction("Index");
            }

            return View();
        }

    }
}