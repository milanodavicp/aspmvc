﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCApp.Models
{
    public class ProizvodiModel
    {
        public int Id { get; set; }

        [Display(Name = "Proizvod Id")]
        [Range(100000, 999999, ErrorMessage = "Unesi ispravan id")]
        public int ProizvodId { get; set; }
        [Required(ErrorMessage = "mora se uneti naziv")]
        public string naziv { get; set; }
        public string opis { get; set; }
        public string kategorija { get; set; }
        public string proizvodjac { get; set; }
        public string dobavljac { get; set; }
        public int cena { get; set; }
    }
}